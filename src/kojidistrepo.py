"""Koji callback for triggering Nomad jobs (which then run koji dist-repo)"""

import logging
import base64
import json
from configparser import ConfigParser
import re
import requests
import simplejson
import koji
from koji import PluginError
from koji.plugin import callback, ignore_error
import kojihub


CONFIG_FILE = '/etc/koji-hub/plugins/distrepo.conf'
PLUGIN_NAME = 'koji.plugin.kojidistrepo'
DEFAULT_ARCHES = 'x86_64 aarch64'
logger = logging.getLogger(PLUGIN_NAME)

def get_config():
    """ Get the distrepo configuration file """
    config = ConfigParser()
    config.read(CONFIG_FILE)

    if not config.has_section('nomad'):
        config.add_section('nomad')
    if not config.has_option('nomad', 'host'):
        logging.getLogger(PLUGIN_NAME).error('No Nomad host specified in config file!')
        return None
    if not config.has_option('nomad', 'token'):
        logging.getLogger(PLUGIN_NAME).error('No Nomad token specified in config file!')
        return None
    if not config.has_option('nomad', 'job'):
        config.set('nomad', 'job', 'prod_koji-distrepo')
    if not config.has_option('nomad', 'parent'):
        config.set('nomad', 'parent', 'koji')
    if not config.has_option('nomad', 'port'):
        config.set('nomad', 'port', 4646)
    if not config.has_option('nomad', 'secure'):
        config.set('nomad', 'secure', True)
    if not config.has_option('nomad', 'verify'):
        config.set('nomad', 'verify', True)
    if not config.has_option('nomad', 'timeout'):
        config.set('nomad', 'timeout', 5)
    if not config.has_option('nomad', 'exclude_tags'):
        config.set('nomad', 'exclude_tags', '')
    return config


def _dispatch_job(payload):
    """ Dispatch the job """
    config = get_config()
    if not config:
        raise PluginError("""Unable to dispatch job because there is the
                          host or token is not defined""")
    if 'TAG' not in payload:
        logger.info('No tag specified')
        return None

    host = config.get('nomad', 'host')
    port = config.getint('nomad', 'port')
    job = config.get('nomad', 'job')
    parent = config.get('nomad', 'parent')
    token = config.get('nomad', 'token')
    timeout = config.getint('nomad', 'timeout')

    exclude_tags = config.get('nomad', 'exclude_tags')
    if exclude_tags:
        exclude_tags = [x.strip() for x in exclude_tags.split(',')]
    else:
        exclude_tags = []

    regex_tags = '|'.join(exclude_tags)
    if re.search(regex_tags, payload['TAG']) is not None:
        logger.info(f"Tag {payload['TAG']} excluded")
        return None

    data = {'Meta': {'PARENT_JOB': parent},
            'Payload': base64.b64encode(str.encode(json.dumps(payload))).decode()}
    headers = {'X-Nomad-Token': token}

    # Build the URL
    url = "http://"
    if config.getboolean('nomad', 'secure'):
        url = "https://"
    url += f"{host}:{port}/v1/job/{job}/dispatch"

    ssl = config.getboolean('nomad', 'verify')
    if config.has_option('nomad', 'cert'):
        ssl = config.get('nomad', 'cert')

    try:
        logger.info(f"Triggering job {job} with payload {payload}")
        req = requests.post(url, json=data, headers=headers, verify=ssl,
                            timeout=timeout,)
        if req.ok:
            return req.json()
        if req.status_code == 403:
            logger.error(f"Access denied: {req.text}")
        elif req.status_code == 404:
            logger.error(f"URL {url} not found: {req.text}")
        else:
            logger.error(f"Bad request: {req.text}")

    except requests.exceptions.Timeout as err:
        logger.warning(f"Timeout error: {err}")
    except requests.exceptions.SSLError as err:
        logger.error(f"SSL error: {err}")
    except requests.exceptions.RequestException as err:
        logger.error(f"Bad request: {err}")
    except simplejson.scanner.JSONDecodeError:
        logger.error(f"Received invalid response: {req.text}")
    return None


def _get_build_target(task_id):
    """Get build target"""
    try:
        task = kojihub.Task(task_id)
        info = task.getInfo(request=True)
        request = info['request']
        if info['method'] in ('build', 'maven'):
            # request is (source-url, build-target, map-of-other-options)
            if request[1]:
                return kojihub.get_build_target(request[1])
        elif info['method'] == 'winbuild':
            # request is (vm-name, source-url, build-target, map-of-other-options)
            if request[2]:
                return kojihub.get_build_target(request[2])
    except Exception as err:
        logger.error(f"Exception: {err}")
    return None


@callback('postTag', 'postUntag')
@ignore_error
def tag_handler(cbtype, *args, **kws):
    """Tag handler"""
    logger.debug(f"Called the {cbtype} callback, args: {str(args)}; kws: {str(kws)}")

    tag = kws['tag']['name']
    build_task_id = kws['build']['task_id']

    build_target = _get_build_target(build_task_id)
    logger.debug(f"Build target: {build_target}")

    arches = DEFAULT_ARCHES
    if build_target:
        build_tag = kojihub.get_tag(build_target['build_tag_name'])
        arches = build_tag['arches']

    payload = {'TAG': tag, 'ARCHES': arches}
    job = _dispatch_job(payload)
    if job:
        logger.info(f"Nomad job info: {job}")


@callback('preBuildStateChange', 'postBuildStateChange')
@ignore_error
def cancel_handler(cbtype, *args, **kws):
    """Cancel tag handler"""
    logger.debug(f"Called the {cbtype} callback, args: {str(args)}; kws: {str(kws)}")
    tagcache = {}

    # We're only interested in canceled builds
    if kws['new'] != koji.BUILD_STATES['CANCELED']:
        return

    build_task_id = kws['info']['task_id']

    # We need to save the list of tags now, before they're deleted
    if cbtype == 'preBuildStateChange':
        tags = list(kojihub.list_tags(build=kws['info']['nvr']))
        tagcache[build_task_id] = tags
        return

    # If we're here, we're in postBuildStateChange
    # Figure out the arches
    build_target = _get_build_target(build_task_id)
    arches = DEFAULT_ARCHES
    if build_target:
        build_tag = kojihub.get_tag(build_target['build_tag_name'])
        arches = build_tag['arches']

    for tag in tagcache.get(build_task_id, []):
        payload = {'TAG': tag['name'], 'ARCHES': arches}
        job = _dispatch_job(payload)
        if job:
            logger.info(f"Nomad job info: {job}")

    # Delete the cache
    del tagcache[build_task_id]
