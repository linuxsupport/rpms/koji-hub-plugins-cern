# Koji callback for GPG signing RPMs before import
# Consider sigul for a more robust solution -- https://fedorahosted.org/sigul/
#
# Author:
#     Paul B Schroeder <paulbsch "at" vbridges "dot" com>
#
# Modified by Thomas Oulevey "at" cern "dot" ch

from kojihub import get_buildroot
from koji.plugin import register_callback
import logging
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser as ConfigParser
from koji import pathinfo
import sys
from subprocess import PIPE, STDOUT, run
import pty
import os
import tempfile
import hashlib

# Configuration file in /etc like for other plugins
config_file = '/etc/koji-hub/plugins/sign.conf'
MAX_ATTEMPTS = 5

def sign(cbtype, *args, **kws):
    if kws['type'] not in ['build', 'rpm']:
       return

    # Get the tag name from the buildroot map
    sys.path.insert(0, '/usr/share/koji-hub')
    if 'rpm' in kws['type']:
        tag_name="DEFAULT"
    else:
        br_id = list(kws['brmap'].values())[0]
        br = get_buildroot(br_id)
        tag_name = br['tag_name']

    logger = logging.getLogger('koji.plugin.sign')
    logger.info("Got package with tag_name %s", tag_name)

    # Get GPG info using the config for the tag name
    config = ConfigParser()
    config.read(config_file)
    if not config.has_section(tag_name):
        logger.info("[WARN] %s not defined in %s" % (tag_name,config_file))
        tag_name="DEFAULT"
    rpm = config.get(tag_name, 'rpm')
    gpgbin = config.get(tag_name, 'gpgbin')
    gpg_path = config.get(tag_name, 'gpg_path')
    gpg_name = config.get(tag_name, 'gpg_name')
    gpg_pass = config.get(tag_name, 'gpg_pass')
    try:
        enabled = config.getboolean(tag_name, 'enabled')
    except:
        # Note that signing is _enabled_ by default
        enabled = True


    if not enabled:
        logger.info('Signing not enabled for this tag.')
        return

    # Get the package paths set up
    uploadpath = pathinfo.work()
    rpms = ''
    if 'rpm' in kws['type']:
       rpms = kws['filepath']
    else:
       for relpath in [kws['srpm']] + kws['rpms']:
           rpms += '%s/%s ' % (uploadpath, relpath)

    # Get the packages signed
    os.environ['LC_ALL'] = 'C'
    logger.info('Attempting to sign packages'
       ' (%s) with key "%s"' % (rpms, gpg_name))
    rpm_cmd = [rpm, "--resign", "--define", "_signature gpg"]
    rpm_cmd.extend(["--define", "_gpgbin %s" % gpgbin])
    rpm_cmd.extend(["--define", "_gpg_path %s" % gpg_path])
    rpm_cmd.extend(["--define", "__gpg_sign_cmd %%{__gpg} gpg --batch --pinentry-mode loopback --passphrase '%s' --no-permission-warning --no-secmem-warning -u '%%{_gpg_name}' -sbo %%{__signature_filename} %%{__plaintext_filename}" % gpg_pass])
    rpm_cmd.extend(["--define", "_gpg_name %s" % gpg_name])
    rpm_cmd.extend(rpms.split())

    # Add rpm output to a temporary file
    log = tempfile.mktemp()
    attempt = 0
    with open(log, 'w') as fout:
        while attempt < MAX_ATTEMPTS:
            # Lets log the MD5 hashes of the rpms, in case it helps with debugging
            fout.write(f"MD5 hashes:\n")
            for rpm in rpms.split():
                hash = hashlib.md5(open(rpm,'rb').read()).hexdigest()
                fout.write(f"{hash}  {rpm}\n")
            fout.write('\n')

            log_cmd = ' '.join(rpm_cmd).replace(gpg_pass, '***')
            logger.info('(attempt %d) Running: %s' % (attempt, log_cmd))
            fout.write('(attempt %d) Running: %s\n\n' % (attempt, log_cmd))
            master_fd, slave_fd = pty.openpty()
            output = run(rpm_cmd, stdin=slave_fd, stdout=PIPE, stderr=STDOUT, timeout=300)
            fout.write(output.stdout.decode())

            if output.returncode == 0:
                fout.write('Package(s) signed successfully!\n')
                fout.write(f"New MD5 hashes:\n")
                for rpm in rpms.split():
                    hash = hashlib.md5(open(rpm,'rb').read()).hexdigest()
                    fout.write(f"{hash}  {rpm}\n")
                logger.info('Package(s) signed successfully!')
                break
            else:
                fout.write(f"Attempt {attempt} failed, return code {output.returncode}.\n\n\n")
                logger.error('Unexpected signing result!')
                logger.error('%s: %s' % (output.returncode, output.stdout.decode()))
                attempt += 1
        else:
            raise Exception("Unexpected signing result! Return code %d, logs in %s" % (output.returncode, log))

register_callback('preImport', sign)

