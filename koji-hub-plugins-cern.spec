Name:		koji-hub-plugins-cern
Version:	1.10
Release:	1%{?dist}
Summary:	Koji hub plugins used at CERN
Vendor:		CERN
Group:		Applications/System
License:	GPLv2+
BuildArch:	noarch
URL:		http://linux.cern.ch/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:	%{name}-%{version}.tgz

Requires:   koji-hub-plugins
Requires:   gnupg2
Requires:   rpm
Requires:   rpm-sign
Requires:   selinux-policy-targeted

Requires: python3-requests
Requires: python3-simplejson
Requires: python3-policycoreutils
BuildRequires: python3-policycoreutils
Requires: python3-pexpect

BuildRequires: checkpolicy, policycoreutils
BuildRequires: selinux-policy-devel

%description
Koji hub plugins used at CERN

%define semodule httpdtty
%define policy_dir /usr/share/selinux/targeted

%prep

%setup -q

%build
make -f /usr/share/selinux/devel/Makefile

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc/koji-hub/plugins
mkdir -p %{buildroot}/usr/lib/koji-hub-plugins
mkdir -p %{buildroot}%{policy_dir}
install -m 0644 src/sign.conf %{buildroot}/etc/koji-hub/plugins
install -m 0644 src/sign.py %{buildroot}/usr/lib/koji-hub-plugins
install -m 0644 src/distrepo.conf %{buildroot}/etc/koji-hub/plugins
install -m 0644 src/kojidistrepo.py %{buildroot}/usr/lib/koji-hub-plugins
cp %{semodule}.pp %{buildroot}%{policy_dir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) /etc/koji-hub/plugins/sign.conf
/usr/lib/koji-hub-plugins/sign.py*
%config(noreplace) /etc/koji-hub/plugins/distrepo.conf
/usr/lib/koji-hub-plugins/kojidistrepo.py*
%{policy_dir}/%{semodule}.pp

%post
semodule -i %{policy_dir}/%{semodule}.pp
semodule -R

%postun
semodule -R

%changelog
* Fri Jan 10 2025 Ben Morrice <ben.morrice@cern.ch> - 1.10-1
- add support to sign imported RPMs

* Thu May 4 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 1.9-1
- check exclude tags in a regex format
- remove mash-triggering plugin

* Wed Apr 26 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 1.8-2
- remove i386 from the defaul arches in kojidistrepo plugin

* Thu Mar 30 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> - 1.8-1
- added kojidistrepo-triggering plugin

* Tue Mar 07 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.7-1
- retry gpg signing multiple times, fix security issue, remove warnings

* Tue Mar 16 2021 Alex Iribarren <alex.iribarren@cern.ch> - 1.6-6
- increase the gpg timeout

* Wed Sep 9 2020 Alex Iribarren <alex.iribarren@cern.ch> - 1.6-1
- actually make it python 3 compatible

* Wed Sep 9 2020 Alex Iribarren <alex.iribarren@cern.ch> - 1.5-2
- build for CentOS 8

* Tue Nov 26 2019 Alex Iribarren <alex.iribarren@cern.ch> - 1.5-1
- fix build target identification

* Fri Jul 19 2019 Alex Iribarren <alex.iribarren@cern.ch> - 1.4-1
- added exclude_tags config option

* Fri Feb 15 2019 Alex Iribarren <alex.iribarren@cern.ch> - 1.3-1
- added "cancel build" support to mash-triggering plugin
- don't fail when tagging binary builds, default to building for all arches

* Wed Nov 21 2018 Alex Iribarren <alex.iribarren@cern.ch> - 1.2-1
- added mash-triggering plugin

* Thu Jul 26 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.1-1
- include correct selinux policy
- fix debug message

* Fri Jul 13 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-3
- Fix spec file for pyo pyc

* Tue Jul 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-2
- Add rpm-sign as a dependency

* Tue Jul 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-1
- Initial version
